//
//  NSDictionary+Util.m
//  GDataInterface
//
//  Created by Peter Easdown on 4/01/12.
//  Copyright (c) 2012 PKCLsoft. All rights reserved.
//
//  This code gratefully adapted from:
//
//  http://www.cocoanetics.com/2009/09/nsdictionary-from-nsdata/
//

#import "NSDictionary+Util.h"

@implementation NSDictionary (Util)

+ (NSDictionary *)dictionaryWithContentsOfData:(NSData *)data {
	// uses toll-free bridging for data into CFDataRef and CFPropertyList into NSDictionary
	CFPropertyListRef plist =  CFPropertyListCreateFromXMLData(kCFAllocatorDefault, (CFDataRef)data,
															   kCFPropertyListImmutable,
															   NULL);
    
	// we check if it is the correct type and only return it if it is
	if ([(id)plist isKindOfClass:[NSDictionary class]])
	{
		return [(NSDictionary *)plist autorelease];
	}
	else
	{
		// clean up ref
		CFRelease(plist);
		return nil;
	}
}

+ (NSData *)dataWithContentsOfDictionary:(NSDictionary *)dict {
	// uses toll-free bridging for data into CFDataRef and CFPropertyList into NSDictionary
    CFDataRef data = CFPropertyListCreateXMLData(kCFAllocatorDefault, dict);
    
	// we check if it is the correct type and only return it if it is
	if ([(id)data isKindOfClass:[NSData class]])
	{
		return [(NSData *)data autorelease];
	}
	else
	{
		// clean up ref
		CFRelease(data);
        
		return nil;
	}
}

@end
