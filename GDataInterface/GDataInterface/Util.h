//
//  Util.h
//  GDataInterface
//
//  This is simply a small utility class containing some miscellansous convenience functions.
//
//  Created by Peter Easdown on 19/12/11.
//  Copyright (c) 2011 PKCLsoft. All rights reserved.
//

#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
#else
#import <Cocoa/Cocoa.h>
#endif

@interface Util : NSObject {
	
}

+ (NSMutableString*) getPassword:(NSString*)forKey;

+ (void) setPassword:(NSString*)newPassword forKey:(NSString*)forKey;

+ (void) wipe:(NSMutableString*)password;
	
+ (NSData*) decryptedData:(NSData*)theData;
+ (NSData*) encryptedData:(NSData*)theData;
+ (NSData*) encryptedDataForKey:(NSString*)forKey;

// Displays the specified alert text.  Works on iOS and MacOS.
//
+ (void)displayAlert:(NSString *)title forWindow:(id)window format:(NSString *)format, ...;

@end
