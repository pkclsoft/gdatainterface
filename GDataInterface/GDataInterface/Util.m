//
//  Util.m
//  GDataInterface
//
//  Created by Peter Easdown on 19/12/11.
//  Copyright (c) 2011 PKCLsoft. All rights reserved.
//

#import "Util.h"
#import "NSData-Util.h"


@implementation Util

#define KEY @"hdalhgar;fbdjk;dfanj;"

+ (double) getVersion {
	// Get the application bundle version
	NSString *version = [[[NSBundle bundleForClass:[self class]] infoDictionary] objectForKey:@"CFBundleVersion"]; 
	return [version doubleValue];
}

+ (NSData*) decryptedData:(NSData*)theData {
	return [theData AES256DecryptWithKey:KEY];
}

+ (NSMutableString*) getPassword:(NSString*)forKey {
	NSMutableString *password = nil;
	
	NSData* encryptedPassword = [Util encryptedDataForKey:forKey] ;
	
	if (![encryptedPassword isKindOfClass:[NSData class]]) {
		return nil;
	}
	
	if (encryptedPassword != nil) {
		password = [[[NSMutableString alloc] initWithData:[Util decryptedData:encryptedPassword] 
												 encoding:NSUTF8StringEncoding] autorelease];
	}
	
	return password;
}

+ (NSData*) encryptedData:(NSData*)theData {
	return [theData AES256EncryptWithKey:KEY];
}

+ (NSData*) encryptedDataForKey:(NSString*)forKey {
	return [[NSUserDefaults standardUserDefaults] valueForKey:forKey];
}

+ (void) setPassword:(NSString*)newPassword forKey:(NSString*)forKey {
	NSData* passwordData = [newPassword dataUsingEncoding:NSUTF8StringEncoding];
	
	[[NSUserDefaults standardUserDefaults] setValue:[Util encryptedData:passwordData] forKey:forKey];
}

+ (void) wipe:(NSMutableString*)password {
	// Overwrite the password with zero's so that the memory isn't left intact with the
	// plain text password.
	//
	if (password != nil) {
		NSMutableData *blank = [[[NSMutableData alloc] initWithLength:[password length]] autorelease];
		NSString *blankString = [[[NSString alloc] initWithData:blank encoding:NSUTF8StringEncoding] autorelease];
		NSRange range = {0, [password length]};
		
		[password replaceCharactersInRange:range withString:blankString];
	}
}

// Displays the specified alert text.  Works on iOS and MacOS.
//
+ (void)displayAlert:(NSString *)title forWindow:(id)window format:(NSString *)format, ... {
    NSString *result = format;
    if (format) {
        va_list argList;
        va_start(argList, format);
        result = [[[NSString alloc] initWithFormat:format
                                         arguments:argList] autorelease];
        va_end(argList);
    }
    
#if TARGET_OS_IPHONE
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:result delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
#else
    NSBeginAlertSheet(title, nil, nil, nil, window, nil, nil,
                      nil, nil, result);
#endif
}

@end
