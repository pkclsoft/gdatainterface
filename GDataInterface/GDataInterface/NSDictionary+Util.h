//
//  NSDictionary+Util.h
//  GDataInterface
//
//  Created by Peter Easdown on 4/01/12.
//  Copyright (c) 2012 PKCLsoft. All rights reserved.
//
//  This code gratefully adapted from:
//
//  http://www.cocoanetics.com/2009/09/nsdictionary-from-nsdata/
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Util)

+ (NSDictionary *)dictionaryWithContentsOfData:(NSData *)data;

+ (NSData *)dataWithContentsOfDictionary:(NSDictionary *)dict;

@end
