//
//  NSData-Util.h
//  Encryption
//
// The AES code was obtained from:
//
// http://pastie.org/426530
//
// which was posted by: http://www.blogger.com/profile/12197600456029078788
//

#import <Foundation/Foundation.h>
#if TARGET_OS_IPHONE
#else
#import <Cocoa/Cocoa.h>
#endif

@interface NSData(Util)

- (NSData *) AES256EncryptWithKey:(NSString *)key;
- (NSData *) AES256DecryptWithKey:(NSString *)key;
- (NSString*) md5;

@end
